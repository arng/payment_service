﻿using FluentValidation;
using FluentValidation.Attributes;
using ServiceStack;

namespace Payment_Service.ServiceModel
{
    [Route("/charge", "POST")]
    public class Charge : IReturn<ChargeResponse>
    {
        public string ProviderName { get; set; }
        public ProviderChargeContext ChargeDetails { get; set; }





        

    }

    public class ChargeResponse
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

    }
