﻿using ServiceStack.FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceModel.Validation
{
    public class ProviderChargeContextValidator : AbstractValidator<ProviderChargeContext>
    {
        public ProviderChargeContextValidator()
            {
            RuleFor(x => x.Amount).NotNull();
            RuleFor(x => x.Currency).NotNull();
            RuleFor(x => x.ProviderCard).SetValidator(new ProviderCardContextValidator());

        }

    }
}
