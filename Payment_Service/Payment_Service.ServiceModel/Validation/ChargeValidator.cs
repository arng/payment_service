﻿using ServiceStack;
using ServiceStack.FluentValidation;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceModel.Validation
{
    public class ChargeValidator: AbstractValidator<Charge>
    {
        public ChargeValidator()
        {
            RuleFor(x => x.ProviderName).NotNull();
            RuleFor(x => x.ChargeDetails).SetValidator(new ProviderChargeContextValidator());


        }

    }
}
