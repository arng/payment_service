﻿using ServiceStack.FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceModel.Validation
{
    public  class ProviderCardContextValidator : AbstractValidator<ProviderCardContext>
    {
        public ProviderCardContextValidator()
        {
            RuleFor(x => x.Number).NotNull();
            RuleFor(x => x.Name).NotNull();
            RuleFor(x => x.LastName).NotNull();
            RuleFor(x => x.ExpirationMonth).NotNull();
            RuleFor(x => x.ExpirationYear).NotNull();
            RuleFor(x => x.Cvc).NotNull();


        }
    }
}
