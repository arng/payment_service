﻿using Payment_Service.Data;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceModel
{
    [Route("/ChargeById/{Id}")]
    public class ChargeById : IReturn<ChargeByIdResponse>
    {
        public string  Id { get; set; }
    }

    public class ChargeByIdResponse
    {
        public ChargeDbModel ChargeModel { get; set; }
    }
}
