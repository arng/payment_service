﻿namespace Payment_Service.ServiceModel
{
    public class ProviderChargeContext
    {

        public int Amount { get; set; }
        public string Currency { get; set; }
        public string UserId { get; set; }  // Used to track user, can be leaved empty for annonymous charge
        public string Description { get; set; }
        public ProviderCardContext ProviderCard { get; set; }
    }
}