﻿using ServiceStack;
using System.Runtime.InteropServices;

namespace Payment_Service.ServiceModel
{
    public class Refund : IReturn<RefundResponse>
    {
        public string ProviderName { get; set; }
        public string id { get; set; }
    }

    public class RefundResponse
    {
        public string status { get; set; }
    }
}