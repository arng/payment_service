﻿using System.Collections.Generic;
using ServiceStack;
using Payment_Service.Data;

namespace Payment_Service.ServiceModel
{
    [Route("/GetTransactions", "GET")]
    
    public class GetTransactions :IReturn<GetTransactionsResponse>
    {
        public int Count { get; set; }
        public string ProviderName { get; set; }

    }

    public class GetTransactionsResponse
    {
        private List<ChargeDbModel> Transactions { get; set; }
    }
}