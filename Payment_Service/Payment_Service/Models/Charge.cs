﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payment_Service.Models
{
    public class Charge
    {
        [Required]
        public string ProviderName { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public string Description { get; set; }

        [Required]
        public string Number { get; set; }
        [Required]
        public int ExpirationYear { get; set; }
        [Required]
        public int ExpirationMonth { get; set; }
        public string AddressCountry { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressZip { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Cvc { get; set; }

    }
}