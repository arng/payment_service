﻿

using System.ComponentModel.DataAnnotations;

namespace Payment_Service.Models
{
    public class Register
    {

        [Required]

        public string Email { get; set; }
        [Required]
        [StringLength(5)]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public bool? AutoLogin { get; set; }
 
        public string Continue { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}