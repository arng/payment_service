﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceStack.Mvc;
using Payment_Service.Models;
using ServiceStack;
using ServiceStack.Auth;
using System.Web.Security;
using Payment_Service.ServiceInterface;
using Payment_Service.ServiceModel;
using Payment_Service.ModelConvertions;
using System.Linq.Expressions;
using System.Web.Helpers;
using Payment_Service.BusinessLogic;
using Payment_Service.ServiceInterface.AdminServices;
using Payment_Service.ServiceModel.AdminModels;

namespace Payment_Service.Controllers
{
    [Authenticate]
    [RequiredRole("Admin")]
    public class AdminController : ServiceStackController
    {



        public ActionResult Index()
        {
            return View();







            }

        public ActionResult Users(string sortBy, string orderDirection = "ascending")
        {
        
            ViewBag.OrderDirection = orderDirection == "ascending" ? "descending" : "ascending";

            using (var GetUserService = ResolveService<GetUsersService>())
            {
                var response = GetUserService.Get(new GetUSers() { Limit = 10 });
                return View(response.Users.AsQueryable().OrderBy(sortBy, EnumHelper.Parse<SortDirection>(orderDirection)).ToList());
            }
        }






        //private void GiveUserAdmin(string username)
        //{
        //    var db = (new MongoClient(WebConfigurationManager.AppSettings["MongoConnectionString"] + "PaymentService")).GetDatabase("PaymentService");
        //    var collection = db.GetCollection<UserAuth>("UserAuth");
        //    var user = collection.Find(x => x.UserName == "test").SingleOrDefault();
        //    user.Roles = new List<string>() { "Admin" };
        //    var filter = Builders<UserAuth>.Filter.Eq(x => x.Id, user.Id);
        //    collection.ReplaceOne(filter, user);
        //}

    }
}