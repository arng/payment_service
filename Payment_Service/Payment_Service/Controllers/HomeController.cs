﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceStack.Mvc;
using Payment_Service.Models;
using ServiceStack;
using ServiceStack.Auth;
using System.Web.Security;
using Payment_Service.ServiceInterface;
using Payment_Service.ServiceModel;
using Payment_Service.ModelConvertions;
using System.Linq.Expressions;
using System.Web.Helpers;
using Payment_Service.BusinessLogic;

namespace Payment_Service.Controllers
{
    [Authenticate]
    public class HomeController : ServiceStackController
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Transactions(string sortBy, string providerName,string orderDirection= "ascending")
        {
            ViewBag.OrderDirection = orderDirection == "ascending" ? "descending" : "ascending";
            ViewBag.ProviderNameParm = providerName;
            using (var transactionService = ResolveService<GetTransactionsService>())
            {
                var response = transactionService.Get(new GetTransactions() {Count = 10,ProviderName = providerName});

                var sortedResponse=response.AsQueryable().OrderBy(sortBy,EnumHelper.Parse<SortDirection>(orderDirection)).ToList();

                return View(sortedResponse);
            }
        }


        [ErrorHandling]
        public ActionResult Charge(string id) // Make only go to this action if user gives id.
        {

            using (var ChargeByIdService = ResolveService<ChargeByIdService>())
            {
                var response = ChargeByIdService.Get(new ChargeById() { Id = id });
                return View(response.ChargeModel);
            }
        }

        public ActionResult Pay()
        {
            return View(new Models.Charge());
        }
        [HttpPost]
        public ActionResult Pay(Models.Charge charge)
        {
            using (var service = ResolveService<ChargeService>())
            {
                var response = service.Post(ModelsConvertions.ViewChargeToServiceCharge(charge));
            }
            return RedirectToAction("Transactions");
        }

        public ActionResult Refund(string providerName,string id)
        {
            using (var refundService = ResolveService<RefundService>())
            {
                var response = refundService.Post(new Refund {id =id,ProviderName= providerName });
            }
            return RedirectToAction("Transactions");
        }

        public ActionResult About()
        {
            ViewBag.Message = GetSession().FirstName;
            return View();
        }
      
        
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


    }
}