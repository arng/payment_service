﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceStack.Mvc;
using Payment_Service.Models;
using ServiceStack;
using ServiceStack.Auth;
using System.Web.Security;
using Payment_Service.Attributes;

namespace Payment_Service.Controllers
{
    
    public class UserController : ServiceStackController
    {

        private JsonServiceClient client;

        public UserController()
        {
            client = new JsonServiceClient("http://localhost:57177/api/");
        }
 
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View(new Models.Register());
        }

        public ActionResult Login()
        {
            return View(new Login());
        }
        public ActionResult Logout()
        {
            base.Gateway.Publish(new Authenticate { provider = "logout" });
            FormsAuthentication.SignOut();

            return Redirect("/");
        }

      

        [AllowAnonymous]
        [HttpPost]
        [ActionName("Login")]
        [ValidateModelState]
        public ActionResult Login(Login model)
        {
     
                Authenticate(model.UserName, model.Password);
                return RedirectToAction("Index", "Home");

        }
        public string Authenticate(string username, string password)
        {
            using (var authService = ResolveService<AuthenticateService>())
            {
                try
                {
                    var response = authService.Authenticate(new Authenticate
                    {
                        provider = CredentialsAuthProvider.Name,
                        UserName = username,
                        Password = password,
                        RememberMe = true,
                    });
                    FormsAuthentication.SetAuthCookie(username, true);
                    return "Success";
                }
                catch (Exception e)// TOOD change to attribute 
                {
                    //TempData["error"] = e.Message;
                    return e.Message;
                }
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [ActionName("Register")]
        [ValidateModelState]
        public ActionResult Register(Models.Register model)
        {
                try
                {
                    var a = client.Post(new ServiceStack.Register //TODO change way of posting 
                    {
                        AutoLogin = true,
                        DisplayName = model.FirstName,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        UserName = model.UserName,
                        Email = model.Email,
                        Password = model.Password,
                        Continue = "https://google.lt"
                    });
                    Authenticate(model.UserName, model.Password);
                    var f = GetSession();



                    //  var RedirectUrl = Url.Action("TransactionList", "Auth");
                    return RedirectToAction("Index", "Home"); ;
                }
                catch (Exception e)// TOOD change to attribute 
                {
                    TempData["message"] = e.Message;
                    return View("Register", new Models.Register());
                }
            }
      
        }
    }

