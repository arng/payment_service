﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Payment_Service
{
    public class ErrorHandling : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {

            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            //var model = new HandleErrorInfo(filterContext.Exception, "Controller", "Action");

            var currentController = (string)filterContext.RouteData.Values["controller"];
            var currentActionName = (string)filterContext.RouteData.Values["action"];
          

            filterContext.Result = new ViewResult()
        {
            ViewName = currentActionName,   
            ViewData = filterContext.Controller.ViewData,
            MasterName = Master,                
            TempData = filterContext.Controller.TempData
        };
        }
    }
}