﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Payment_Service
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("api/{*pathInfo}");


            //    routes.MapRoute(
            //    "Transactions",                                           
            //    "Home/Transactions", // Todo make it better                          
            //    new { controller = "Home", action = "Refund" }
            //);


            routes.MapRoute(
                "Refund",                                           
                "refund/{providerName}/{id}", // Todo make it better                          
                new { controller = "Home", action = "Refund" }
            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
