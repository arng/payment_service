﻿using Payment_Service.ServiceModel;
using Payment_Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payment_Service.ModelConvertions
{
    public static class ModelsConvertions
    {

        public static ServiceModel.Charge ViewChargeToServiceCharge(Models.Charge charge)
        {
            return new ServiceModel.Charge
            {
                ProviderName = charge.ProviderName,
                ChargeDetails = new ProviderChargeContext
                {
                    Amount = charge.Amount,
                    Currency = charge.Currency,
                    Description = charge.Description,
                    ProviderCard = new ProviderCardContext
                    {
                        Name = charge.Name,
                        AddressCity = charge.AddressCity,
                        Cvc = charge.Cvc,
                        AddressCountry = charge.AddressCountry,
                        AddressLine1 = charge.AddressLine1,
                        AddressLine2 = charge.AddressLine2,
                        AddressState = charge.AddressState,
                        AddressZip = charge.AddressZip,
                        ExpirationMonth = charge.ExpirationMonth,
                        ExpirationYear = charge.ExpirationYear,
                        LastName = charge.LastName,
                        Number = charge.Number
                    }
                }
            };
        }

    }
}