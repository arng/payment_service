﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payment_Service.BusinessLogic
{
    public static class EnumHelper
    {
        public static T Parse<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

    }
}