﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Funq;
using ServiceStack;
using ServiceStack.Mvc;
using Payment_Service.ServiceInterface;
using ServiceStack.Auth;
using ServiceStack.Caching;
using System.Configuration;
using MongoDB.Driver;
using System.Web.Configuration;
using ServiceStack.Validation;
using Payment_Service.ServiceModel.Validation;

namespace Payment_Service
{
    public class AppHost : AppHostBase
    {
        /// <summary>
        /// Base constructor requires a Name and Assembly where web service implementation is located
        /// </summary>
        public AppHost()
            : base("Payment_Service", typeof(ChargeService).Assembly) { }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        public override void Configure(Container container)
        {
            SetConfig(new HostConfig
            {
                HandlerFactoryPath = "api",
                DebugMode = true  // true to display StackTraces in Response DTOs
            });

            ControllerBuilder.Current.SetControllerFactory(
           new FunqControllerFactory(container));


            //Config examples
            this.Plugins.Add(new PostmanFeature());


            Plugins.Add(new AuthFeature(() => new AuthUserSession(),
    new IAuthProvider[] {
                       new CredentialsAuthProvider(),
        //HTML Form post of UserName/Password credentials

    })
            {
                HtmlRedirect = "/User/Index"
            }
    );

            Plugins.Add(new RegistrationFeature());

            container.Register<ICacheClient>(new MemoryCacheClient());
            var database = (new MongoClient(WebConfigurationManager.AppSettings["MongoConnectionString"] + "PaymentService")).GetDatabase("PaymentService");
            container.Register<IUserAuthRepository>(c => new MongoDbAuthRepository(database, true));
            var authRepo = (MongoDbAuthRepository)container.Resolve<IUserAuthRepository>();
            authRepo.CreateMissingCollections();


            //validation
            Plugins.Add(new ValidationFeature());
            container.RegisterValidators(typeof(ChargeValidator).Assembly);


            //this.Plugins.Add(new CorsFeature());

            //Set MVC to use the same Funq IOC as ServiceStack
            ControllerBuilder.Current.SetControllerFactory(new FunqControllerFactory(container));
        }
    }
}