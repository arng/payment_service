﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Payment_Service.ServiceInterface.PaymentProviderFactory;
using Payment_Service.ServiceInterface.PaymentProviderFactory.Providers;
using Payment_Service.ServiceModel;
using ServiceStack;
using ServiceStack.Auth;

namespace Payment_Service.ServiceInterface
{
    [Authenticate]
    public class ChargeService : Service
    {
        private AbstractProvider _provider;

        public ChargeResponse Post(Charge request)
        {
            request.ChargeDetails.UserId = this.GetSession().UserAuthId; 
            
            _provider = Factory.Initialize(request.ProviderName);

             return _provider.Charge(request.ChargeDetails);
        }
    }
}
