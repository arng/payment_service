﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Payment_Service.ServiceInterface.PaymentProviderFactory.Providers;

namespace Payment_Service.ServiceInterface.PaymentProviderFactory
{
     static class Factory
    {


        public static AbstractProvider Initialize(string PaymentName)
        {

            switch (PaymentName)
            {
                case "AuthdotNet":
                    return new AuthdotNetProvider();
                case "Stripe":
                    return new StripeProvider();
                default:
                    return null;
            }
            

        }
    }
}
