﻿using System;
using System.Web.Configuration;
using Payment_Service.ServiceModel;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using Payment_Service.Data;
using Payment_Service.ServiceInterface.CoreBusinesslogic;

namespace Payment_Service.ServiceInterface.PaymentProviderFactory.Providers
{
    class AuthdotNetProvider : AbstractProvider
    {

        public AuthdotNetProvider()
        {
         

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = WebConfigurationManager.AppSettings["AuthdotNetApiKey"],
                ItemElementName = ItemChoiceType.transactionKey,
                Item = WebConfigurationManager.AppSettings["AuthdotNetTransactionKey"]
            };
        }

        public override ChargeResponse Charge(ProviderChargeContext chargeContext)
        {
            var collection = _database.GetCollection<ChargeDbModel>("Charges");
            var creditCard = new creditCardType
            {
                cardNumber = chargeContext.ProviderCard.Number,
                expirationDate =chargeContext.ProviderCard.ExpirationMonth+chargeContext.ProviderCard.ExpirationYear.ToString().Substring(0,3),
                cardCode = chargeContext.ProviderCard.Cvc
            };

            var billingAddress = new customerAddressType
            {
                firstName = chargeContext.ProviderCard.Name,
                lastName = chargeContext.ProviderCard.LastName,
                address = chargeContext.ProviderCard.AddressLine1,
                city =chargeContext.ProviderCard.AddressCity,
                zip = chargeContext.ProviderCard.AddressZip,
                country = chargeContext.ProviderCard.AddressCountry,
                state =  chargeContext.ProviderCard.AddressState
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };


            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // charge the card

                amount = chargeContext.Amount,
                payment = paymentType,
                billTo = billingAddress,
                
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();
            ChargeResponse res = new ChargeResponse();
            //validate
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.transactionResponse.messages != null)
                    {
                        res.Id = response.transactionResponse.transId;
                        res.Status = response.transactionResponse.messages[0].description;
                       
                        collection.InsertOne(ObjectConversion.ProviderChargeContextToChargeDbModel(chargeContext, "AuthdotNet", res.Status, res.Id));
                        //Console.WriteLine("Successfully created transaction with Transaction ID: " + response.transactionResponse.transId);
                        //Console.WriteLine("Response Code: " + response.transactionResponse.responseCode);
                        //Console.WriteLine("Message Code: " + response.transactionResponse.messages[0].code);
                        //Console.WriteLine("Description: " + response.transactionResponse.messages[0].description);
                        //Console.WriteLine("Success, Auth Code : " + response.transactionResponse.authCode);
                    }
                    else
                    {
                        Console.WriteLine("Failed Transaction.");
                        if (response.transactionResponse.errors != null)
                        {
                           
                            res.Status = response.transactionResponse.errors[0].errorText;
                            collection.InsertOne(ObjectConversion.ProviderChargeContextToChargeDbModel(chargeContext, "AuthdotNet", res.Status));
                            //Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                            //Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Failed Transaction.");
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        
                        res.Status = response.transactionResponse.errors[0].errorText;
                        collection.InsertOne(ObjectConversion.ProviderChargeContextToChargeDbModel(chargeContext, "AuthdotNet", res.Status));
                        //Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                        //Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                    }
                    else
                    {
                        
                        res.Status = response.messages.message[0].text;
                        collection.InsertOne(ObjectConversion.ProviderChargeContextToChargeDbModel(chargeContext, "AuthdotNet", res.Status));
                        //Console.WriteLine("Error Code: " + response.messages.message[0].code);
                        //Console.WriteLine("Error message: " + response.messages.message[0].text);
                    }
                }
            }
            else
            {
               // Console.WriteLine("Null Response.");
                res.Status = "Null Response.";
            }

            return res;
        }





        /// <summary>
        /// Refunds setlet transaction in AuthorizedotNet.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public override string Refund(string transactionId)
        {

            var transactionDetails = GetTransactionDetails(transactionId);



            var creditCardMasked = (creditCardMaskedType)transactionDetails.transaction.payment.Item;


            var creditCard = new creditCardType
            {
                cardNumber = creditCardMasked.cardNumber,
                expirationDate = creditCardMasked.expirationDate
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };


            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.refundTransaction.ToString(),    // refund type
                payment = paymentType,
                amount = transactionDetails.transaction.authAmount,
                refTransId = transactionId
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.transactionResponse.messages != null)
                    {
                        DbContext.UpdateRefundStatus(transactionId);
                        return response.transactionResponse.messages[0].description;
                    }
                    else
                    {
                        if (response.transactionResponse.errors != null)
                        {
                            return response.transactionResponse.errors[0].errorText;
                        }
                    }
                }
                else
                {
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        return response.transactionResponse.errors[0].errorText;
                    }
                    else
                    {
                        return response.messages.message[0].text;
                    }
                }
            }
            return "";
        }
    

        private getTransactionDetailsResponse GetTransactionDetails(string transactionId)
        {
            var request = new getTransactionDetailsRequest();
            request.transId = transactionId;

            // instantiate the controller that will call the service
            var controller = new getTransactionDetailsController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response.transaction == null)
                    return response;

            }
            else if (response != null)
            {
                //Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                //                  response.messages.message[0].text);
            }

            return response;
        }
    }
    }

