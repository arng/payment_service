﻿using System.Web.Configuration;
using MongoDB.Driver;
using Payment_Service.ServiceModel;
using Stripe;
using Payment_Service.Data;
using System;
using Payment_Service.ServiceInterface.CoreBusinesslogic;

namespace Payment_Service.ServiceInterface.PaymentProviderFactory.Providers
{
    class StripeProvider : AbstractProvider
    {
        

        public StripeProvider()
        {
           
        }

        public override ChargeResponse Charge(ProviderChargeContext chargeContext)
        {
            // setting up the card
            var myCharge = new StripeChargeCreateOptions
            {
                Amount = chargeContext.Amount,
                Currency = chargeContext.Currency,
                Description = chargeContext.Description,
                SourceCard = new SourceCard()
                {
                    Number = chargeContext.ProviderCard.Number,
                    ExpirationYear = chargeContext.ProviderCard.ExpirationYear.ToString(),
                    ExpirationMonth = chargeContext.ProviderCard.ExpirationMonth.ToString("00"),
                    AddressCountry = chargeContext.ProviderCard.AddressCountry, // optional
                    AddressLine1 = chargeContext.ProviderCard.AddressLine1, // optional
                    AddressLine2 = chargeContext.ProviderCard.AddressLine2, // optional
                    AddressCity = chargeContext.ProviderCard.AddressCity, // optional
                    AddressState = chargeContext.ProviderCard.AddressState, // optional
                    AddressZip = chargeContext.ProviderCard.AddressZip, // optional
                    Name = chargeContext.ProviderCard.Name + chargeContext.ProviderCard.LastName, // optional
                    Cvc = chargeContext.ProviderCard.Cvc // optional
                }
            };


            // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
            myCharge.Capture = true;

            var chargeService = new StripeChargeService();
            StripeCharge stripeCharge = chargeService.Create(myCharge);

            var collection = _database.GetCollection<ChargeDbModel>("Charges");
            collection.InsertOne(ObjectConversion.ProviderChargeContextToChargeDbModel(chargeContext, "Stripe", stripeCharge.Status,stripeCharge.Id));//https://domantasjovaisas.wordpress.com/2013/11/29/c-static-explicit-operator/


            // refund button, validation , filtering.
            return new ChargeResponse() {Id=stripeCharge.Id,Status = stripeCharge.Status};

        }

        public override string Refund(string chargeId)
        {
            var refundService = new StripeRefundService();
            var collection = _database.GetCollection<ChargeDbModel>("Charges");
           
            StripeRefund refund = refundService.Create(chargeId, new StripeRefundCreateOptions()
            {
                Amount = collection.Find(x => x.ChargeId == chargeId).SingleOrDefault().Amount,
                Reason = StripeRefundReasons.Fradulent
            });
            if (refund.Status == "succeeded")
            {
                DbContext.UpdateRefundStatus(chargeId);
            }
            return refund.Status;
        }
    }
}
