﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using MongoDB.Driver;
using Payment_Service.Data;
using Payment_Service.ServiceModel;

using MongoDB.Bson;

namespace Payment_Service.ServiceInterface.PaymentProviderFactory.Providers
{
    public abstract class AbstractProvider
    {
        protected IMongoDatabase _database;
        public abstract ChargeResponse Charge(ProviderChargeContext chargeContext);
        public abstract string Refund(string id);

        protected AbstractProvider()
        {
            _database = (new MongoClient(WebConfigurationManager.AppSettings["MongoConnectionString"] + "PaymentService")).GetDatabase("PaymentService");
        }

    


    }
}
