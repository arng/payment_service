﻿using Payment_Service.ServiceInterface.PaymentProviderFactory;
using Payment_Service.ServiceInterface.PaymentProviderFactory.Providers;
using Payment_Service.ServiceModel;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceInterface
{
    [Authenticate]
    public class RefundService:Service
    {
        private AbstractProvider _provider;

        public RefundResponse Post(Refund request)
        {

            _provider = Factory.Initialize(request.ProviderName);

            return new RefundResponse() { status = _provider.Refund(request.id) };
        }
    }
}
