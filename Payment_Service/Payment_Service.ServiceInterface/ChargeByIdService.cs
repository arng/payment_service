﻿using Payment_Service.ServiceInterface.Core_Business_logic_;
using Payment_Service.ServiceInterface.Core_Business_logic_.AccessLevel;
using Payment_Service.ServiceInterface.CoreBusinesslogic;
using Payment_Service.ServiceModel;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceInterface
{
    [Authenticate]
    public class ChargeByIdService: Service
    {

        public ChargeByIdResponse Get(ChargeById request)
        {
            var userId = GetSession().UserAuthId;

            IRole role = Role.Initialize( GetSession().Roles.Exists(x => x == "Admin"));
            var result = DbContext.GetChargeById(request.Id, role);
            if (result== null)
                throw new BusinessException("Something went wrong") ;
            return new ChargeByIdResponse() { ChargeModel = result };
        }
    }
}
