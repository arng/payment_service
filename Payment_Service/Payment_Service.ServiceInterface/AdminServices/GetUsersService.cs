﻿using Payment_Service.ServiceInterface.Core_Business_logic_.AccessLevel;
using Payment_Service.ServiceInterface.CoreBusinesslogic;
using Payment_Service.ServiceModel.AdminModels;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceInterface.AdminServices
{
    [Authenticate]
    [RequiredRole("Admin")]
     public class GetUsersService :Service
    {
        

        public GetUSersResponse Get(GetUSers request)
        {
           

             IRole role = Role.Initialize(true); // change maybe to admin only, not true false

            return (new GetUSersResponse() { Users = DbContext.GetUsers(role) });
        }

    }
}
