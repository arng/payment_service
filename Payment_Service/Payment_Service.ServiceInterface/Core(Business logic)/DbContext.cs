﻿using MongoDB.Driver;
using Payment_Service.Data;
using Payment_Service.ServiceInterface.Core_Business_logic_;
using Payment_Service.ServiceInterface.Core_Business_logic_.AccessLevel;
using ServiceStack.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Payment_Service.ServiceInterface.CoreBusinesslogic
{
    public static class DbContext
    {
        static IMongoDatabase _database;
        static DbContext()
        {

            _database = (new MongoClient(WebConfigurationManager.AppSettings["MongoConnectionString"] + "PaymentService")).GetDatabase("PaymentService");

        }

        public static ChargeDbModel GetChargeById(string idTransaction,IRole user)
        {
            var collection = _database.GetCollection<ChargeDbModel>("Charges");

            var filter = user.GetChargeByIdFilter(idTransaction);

            return collection.Find(filter).SingleOrDefault();
          
        }

        public static List<ChargeDbModel> GetTransactions(string userId, int count, string providername, IRole role) 
        {
            var collection = _database.GetCollection<ChargeDbModel>("Charges");
            var filter = role.GetChargeListFilter();

            List<ChargeDbModel> a = collection.Find(filter)
                .Project<ChargeDbModel>(Builders<ChargeDbModel>
                .Projection
                    .Exclude("UserId")
                    .Exclude("Description"))
                .Limit(count).ToList();


            if (providername== "Stripe"|| providername =="AuthdotNet")
            {
              a =  a.Where(x => x.ProviderName == providername).ToList();
            }
            return a;

        }


        public static void UpdateRefundStatus(string id)
        {
            var collection = _database.GetCollection<ChargeDbModel>("Charges");
            var filter = Builders<ChargeDbModel>.Filter.Eq("ChargeId", id);
            var update = Builders<ChargeDbModel>.Update
                .Set("IsRefunded", true);
            var result =  collection.UpdateOne(filter, update);
        }


        public static List<UserAuth> GetUsers(IRole role)
        {
            var collection = _database.GetCollection<UserAuth>("UserAuth");
            List<UserAuth> a = collection.Find(Builders<UserAuth>.Filter.Empty).ToList();
            return a;
        }

    

     

    }
}
