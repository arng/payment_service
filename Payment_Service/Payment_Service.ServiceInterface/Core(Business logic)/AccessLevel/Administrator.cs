﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using Payment_Service.Data;

namespace Payment_Service.ServiceInterface.Core_Business_logic_.AccessLevel
{
    public class Administrator : IRole
    {

        public string UserId { get; set; }

        public Administrator()
        {

        }

        public Administrator(string id )
        {
            UserId = id;
        }

      

        public FilterDefinition<ChargeDbModel> GetChargeByIdFilter(string idTransaction)
        {
            var builder = Builders<ChargeDbModel>.Filter;
            return builder.Eq("_id", idTransaction);
        }

        public FilterDefinition<ChargeDbModel> GetChargeListFilter()
        {
            return Builders<ChargeDbModel>.Filter.Empty;
           
        }
    }
}
