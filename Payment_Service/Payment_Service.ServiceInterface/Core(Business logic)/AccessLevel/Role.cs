﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceInterface.Core_Business_logic_.AccessLevel
{
   public class Role 
    {
        public static IRole Initialize(bool PaymentName)
        {

            switch (PaymentName)
            {
                case true:
                    return new Administrator();
                case false:
                    return new User();
                default:
                    return null;
            }


        }

        public static IRole Initialize(bool PaymentName,string id)
        {

            switch (PaymentName)
            {
                case true:
                    return new Administrator(id);
                case false:
                    return new User(id);
                default:
                    return null;
            }


        }

    }
}
