﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using Payment_Service.Data;

namespace Payment_Service.ServiceInterface.Core_Business_logic_.AccessLevel
{
    public class User : IRole
    {
        public string UserId { get; set; }

        public User()
        {

        }

        public User(string id)
        {
            UserId = id;
        }

        public FilterDefinition<ChargeDbModel> GetChargeByIdFilter(string idTransaction)
        {
            var builder = Builders<ChargeDbModel>.Filter;
            return builder.Eq("_id", idTransaction) & builder.Eq("UserId", UserId);
        }

        public FilterDefinition<ChargeDbModel> GetChargeListFilter()
        {

            return Builders<ChargeDbModel>.Filter.Eq("UserId", UserId);
        }
    }
}
