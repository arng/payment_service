﻿using MongoDB.Driver;
using Payment_Service.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceInterface.Core_Business_logic_.AccessLevel
{
    public interface IRole
    {
        string UserId { get; set; }
       FilterDefinition<ChargeDbModel> GetChargeByIdFilter(string idTransaction);
       FilterDefinition<ChargeDbModel> GetChargeListFilter();

    }
}
