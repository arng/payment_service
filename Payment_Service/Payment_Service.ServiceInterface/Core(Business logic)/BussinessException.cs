﻿using ServiceStack.FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_Service.ServiceInterface.Core_Business_logic_
{
    [Serializable]
    public class BusinessException : Exception
    {

        public IList<ValidationFailure> Errors { get; set; } = new List<ValidationFailure>();

        public BusinessException(Exception originalException, string messageFormatString, params object[] args)
            : base(string.Format(messageFormatString, args), originalException.GetBaseException())
        {
        }

        public BusinessException()
        {
        }

        public BusinessException(string message, IList<ValidationFailure> errors)
            : base(message)
        {
            Errors = errors;
        }

        public BusinessException(string message)
            : base(message)
        {


        }
    }
    
}
