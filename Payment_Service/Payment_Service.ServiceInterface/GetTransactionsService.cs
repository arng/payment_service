﻿using System.Collections.Generic;
using Payment_Service.ServiceInterface.PaymentProviderFactory;
using Payment_Service.ServiceInterface.PaymentProviderFactory.Providers;
using Payment_Service.ServiceModel;

using ServiceStack;
using Payment_Service.Data;
using Payment_Service.ServiceInterface.CoreBusinesslogic;
using Payment_Service.ServiceInterface.Core_Business_logic_.AccessLevel;

namespace Payment_Service.ServiceInterface
{
    [Authenticate]
    public class GetTransactionsService : Service
    {
      
      

        public List<ChargeDbModel> Get(GetTransactions request)
        {
            var userId = GetSession().UserAuthId;
            IRole role = Role.Initialize(GetSession().Roles.Exists(x => x == "Admin"), userId);
          
            return (DbContext.GetTransactions(userId,request.Count, request.ProviderName,role));
           

        }



    }
}