﻿using System;
using MongoDB.Bson;
namespace Payment_Service.Data
{
    public class Entity
    {
        public Entity()
        {
            Id = ObjectId.GenerateNewId().ToString();
            CreatedOn = DateTime.Now;
        }
        public string Id { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}