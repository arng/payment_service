﻿using System.Globalization;

namespace Payment_Service.Data
{
    public class ChargeDbModel :Entity
    {
        public string  UserId { get; set; }
        public string ChargeId { get; set; }
        public string  ProviderName { get; set; }
        public int     Amount { get; set; }
        public string  Currency { get; set; }
        public string  Description { get; set; }
        public string Status { get; set; }
        public bool IsRefunded { get; set; }


    }
}