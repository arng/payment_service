﻿using Payment_Service.ServiceModel;

namespace Payment_Service.Data
{
    public static class ObjectConversion
    {
        public static ChargeDbModel ProviderChargeContextToChargeDbModel(ProviderChargeContext context, string provider,string status,string chargeId = null)
        {
            return  new ChargeDbModel()
            {
                Amount = context.Amount,
                Currency = context.Currency,
                Description = context.Description,
                ProviderName = provider,
                UserId = context.UserId,
                Status = status,
                ChargeId = chargeId
            };
        }
    }
}